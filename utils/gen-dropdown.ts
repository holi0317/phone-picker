/***
 * Generate a random array of phone number and output to stdout.
 *
 * This script does not run in client because the shuffle is expensive
 * and will slow down the client.
 */

const bag: string[] = [];

for (let i = 0; i < 10000; i++) {
  const str = String(i).padStart(4, "0");
  bag.push(str);
}

bag.sort(() => Math.random() - 0.5);

console.log(JSON.stringify(bag));
