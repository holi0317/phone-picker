import padStart from "lodash-es/padStart";

/**
 * Vue filter for formatting phone number, in Hong Kong format.
 *
 * If the given value is not a number, this will return the
 * given value (I.e. no-op)
 *
 * This will pad the number to 8 digit. If the given number is
 * longer than 8 digit, this will return given value (no-op)
 *
 * Given number could either be in string format or in number format.
 *
 * @param value {String|Number} Phone number to be formatted
 * @param [pad] {boolean} Should the number be padded or not. Default is true.
 * @returns {String} String of formatted number
 */
export default function phoneNumber(value: string | number, pad = true) {
  const num = parseInt(value + "", 10);
  if (isNaN(num)) {
    // Value is not a valid number. Return it
    return value;
  }

  const padded = pad ? padStart(num + "", 8, "0") : num + "";
  if (padded.length > 8) {
    // Value is not a valid phone number
    return value;
  }

  const head = padded.slice(0, 4);
  const tail = padded.slice(4, 8);

  if (head.length === 4) {
    return `${head}-${tail}`;
  } else {
    return head;
  }
}
