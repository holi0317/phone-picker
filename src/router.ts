import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/dropdown",
      name: "dropdown",
      component: () => import("./views/Dropdown.vue")
    },
    {
      path: "/slider",
      name: "slider",
      component: () => import("./views/Slider.vue")
    },
    {
      path: "/random",
      name: "random",
      component: () => import("./views/Random.vue")
    },
    {
      path: "/guess",
      name: "guess",
      component: () => import("./views/Guess.vue")
    },
    {
      path: "/plus-one",
      name: "plus-one",
      component: () => import("./views/PlusOne.vue")
    },
    {
      path: "/plus-n",
      name: "plus-n",
      component: () => import("./views/PlusN.vue")
    },
    {
      path: "/binary",
      name: "binary",
      component: () => import("./views/Binary.vue")
    },
    {
      path: "/random-key",
      name: "random-key",
      component: () => import("./views/RandomKey.vue")
    },
    {
      path: "/reflec-beat",
      name: "reflec-beat",
      component: () => import("./views/ReflecBeat.vue")
    },
    {
      path: "*",
      redirect: "/dropdown"
    }
  ]
});
